/************************************************************************** 
* Verity TI
* --------------------------- 
* Criado por...:           Igor Cunha
* Em...........:           04/04/2017
* Projeto......:           Pause
* Descrição....:           Script para criação das tabelas Despesa e Status banco Pause
* Tabelas envoldidas:      status e despesa
**************************************************************************/ 
DECLARE @nomeScript VARCHAR(MAX);
SET @nomeScript = '01. nome do script aqui'
PRINT 'Iniciando execução script ['+ @nomeScript +']'
BEGIN TRY
    BEGIN TRANSACTION;
		USE [Pause]
		
		CREATE TABLE [PAUSEStatus](
			[id] [numeric](2, 0) IDENTITY(1,1) NOT NULL,
			[nome] [varchar](50) NOT NULL,
			[descricao] [varchar](100) NOT NULL,
		 CONSTRAINT [status_pk] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
		
		CREATE TABLE [PAUSEDespesa](
			[id] [numeric](5, 0) IDENTITY(1,1) NOT NULL,
			[id_status] [numeric](2, 0) NOT NULL,
			[id_tipo_despesa] [numeric](2, 0) NOT NULL,
			[justificativa] [varchar](200) NOT NULL,
			[valor] [numeric](9, 2) NOT NULL,
			[data_solicitacao] [datetime] NOT NULL,
			[id_projeto] [numeric](5, 0) NOT NULL,
			[id_solicitante] [numeric](5, 0) NOT NULL,
			[data_acao_gp] [date] NULL,
			[data_acao_financeiro] [date] NULL,
			[id_gp] [numeric](5, 0) NULL,
			[id_financeiro] [numeric](5, 0) NULL,
			[caminho_comprovante] [varchar](1000) NULL,
			[data_ocorrencia] [datetime] NOT NULL,
			[justificativa_rejeicao] [varchar](500) NULL,
			[reembolso] [bit] NULL,
			[km] [numeric](9, 2) NULL,
		 CONSTRAINT [despesa_pk] PRIMARY KEY CLUSTERED 
		(
			[id] ASC
		)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]

		ALTER TABLE [PAUSEDespesa]  WITH CHECK ADD  CONSTRAINT [despesa_status_fk] FOREIGN KEY([id_status])
		REFERENCES [PAUSEStatus] ([id])

		ALTER TABLE [PAUSEDespesa] CHECK CONSTRAINT [despesa_status_fk]
		
		INSERT INTO [PAUSEStatus]
				   ([nome]
				   ,[descricao])
			 VALUES
				   ('Em análise','Em análise'),
				   ('Aprovado','Despesa aprovada'),
				   ('Reprovado','Despesa reprovada')
		
		COMMIT TRANSACTION;
 
    PRINT 'Sucesso na execução do script ['+ @nomeScript +']'
END TRY
BEGIN CATCH
		IF @@TRANCOUNT > 0
			 ROLLBACK TRANSACTION;
             
 		DECLARE @errorNumber INT;
		SET @errorNumber  = ERROR_NUMBER();
		DECLARE @errorLine INT;
		SET @errorLine = ERROR_LINE();
		DECLARE @errorMessage NVARCHAR(4000);
		SET @errorMessage = ERROR_MESSAGE();
		DECLARE @errorSeverity INT;
		SET @errorSeverity = ERROR_SEVERITY();
		DECLARE @errorState INT;
		SET @errorState = ERROR_STATE();
      
		PRINT 'ERRO na execução do script. [' + @nomeScript + ']'
		PRINT 'Número do erro: [' + CAST(@errorNumber AS VARCHAR(10)) + ']';
		PRINT 'Número da linha: [' + CAST(@errorLine AS VARCHAR(10)) + ']';
		PRINT 'Descrição do erro: [' + @errorMessage + ']';
 
		RAISERROR(@errorMessage, @errorSeverity, @errorState);
END CATCH