package br.com.verity.pause.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.stereotype.Component;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import br.com.verity.pause.bean.DespesaBean;
import br.com.verity.pause.bean.ProjetoBean;

@Component
public class GerarRelatorioDespesaUtil {

	public InputStream gerarRelatorioComprovante(DespesaBean despesa, ProjetoBean projeto) throws DocumentException, IOException {
		// Create PdfReader instance.
		PdfReader templatePDF = new PdfReader(File.separator + "pdf" + File.separator + "rel-despesa.pdf");

		// Create PdfStamper instance.
		ByteArrayOutputStream templateByte = new ByteArrayOutputStream();
		PdfStamper pdfStamper = new PdfStamper(templatePDF, templateByte);

		// Create BaseFont instance.
		BaseFont fontTimes = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.WINANSI, BaseFont.EMBEDDED);

		// Contain the pdf data.
		PdfContentByte pageContentByte = pdfStamper.getOverContent(1);

		pageContentByte.beginText();
		int headerHorizontal = 122;
		// COLABORADOR
		pageContentByte.setFontAndSize(fontTimes, 14);
		pageContentByte.setTextMatrix(headerHorizontal, 632);
		pageContentByte.showText(despesa.getNomeFuncionario());

		// Projeto
		pageContentByte.setTextMatrix(headerHorizontal, 600);
		pageContentByte.showText(projeto.getNome().length()>65?
				projeto.getNome().substring(0, 65):projeto.getNome());

		// Codigo
		pageContentByte.setTextMatrix(headerHorizontal, 567);
		pageContentByte.showText(projeto.getCodigo());
		
		// Empresa
		pageContentByte.setTextMatrix(headerHorizontal, 535);
		pageContentByte.showText(projeto.getEmpresa().getNomeFantasia());
		
		int tabelaVertical = 427;
		pageContentByte.setFontAndSize(fontTimes, 12);
		pageContentByte.setTextMatrix(40, tabelaVertical);
		pageContentByte.showText(despesa.getId().toString());		
									
		pageContentByte.setTextMatrix(96, tabelaVertical);
		pageContentByte.showText(despesa.getDataOcorrencia());
		
		pageContentByte.setTextMatrix(188, tabelaVertical);
		pageContentByte.showText(despesa.getNomeTipoDespesa());
		
		pageContentByte.setTextMatrix(372, tabelaVertical);
		pageContentByte.showText(despesa.getReembolso()?"Reembolso":"Justificativa");
		
		pageContentByte.setTextMatrix(477, tabelaVertical);
		pageContentByte.showText("R$"+despesa.getValor().toString().replace('.', ','));

		pageContentByte.endText();

		// Close the pdfStamper.
		pdfStamper.close();
		InputStream is = new ByteArrayInputStream(templateByte.toByteArray());
		return is;
	}

}
