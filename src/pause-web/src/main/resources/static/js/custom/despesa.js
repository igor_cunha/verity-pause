function abrirModal(){
	$('#add-despesa-modal').modal();
	$("#btnDownloadArquivo").hide();
	$("#btnDownloadArquivoDiv").hide();
	$("#btnDownloadRelComprovante").hide();
	var today = new Date().toISOString().split('T')[0];
	$("#dataDespesa").attr('max', today);
	$("#modal-title").html('Adicionar Despesa');
}
function carregarTipoDespesa(idProjeto, callback){
	var options = [];
	options.push("<option value=''>Selecione</option>");
	$.ajax({
		url: "despesa/tipo-despesa/"+idProjeto,
		type: "get", 
		success: function(data) {
			data.forEach(function (item) {
				var option = "<option value='"+item.id+"'>" + item.nome + "</option>"
				options.push(option);
				});
				$('#select-tipo-despesa').attr('disabled',false);
				$('#select-tipo-despesa').html(options);
				$('#select-tipo-despesa').selectpicker('refresh');
				if(callback != undefined){
					callback();
				}
		},
		error: function(erro) {
			if (erro.status === 403) {                        
				location.reload();                            
			}                                                  
		}
	});
}
function alternarCampoQtdKm(select){
	if($(select).find('option:selected').text().toLowerCase() == 'km'){
		$('#div-qtd-km').removeClass('hide');
		$('#valorDespesa').attr('disabled',true);
	}else{
		$('#div-qtd-km').addClass('hide');
		$('#kmDespesa').val('');
		$('#valorDespesa').attr('disabled',false);
	}
	
}
function calcularValorPorKm(km){
	km = Number(km.replace(',','.'));
	var vl = km * 0.49;
	$('#valorDespesa').val(vl.toFixed(2).toString().replace('.',','));
}
function submeterDespesa() {
	validarFormDespesa();
}

function validarFormDespesa() {
	var erro = false;

	var dataDespesa = $('#dataDespesa').val();
	var valorDespesa = $('#valorDespesa').val();
	var kmDespesa = $('#kmDespesa').val();
	var tipoDespesa = $('#select-tipo-despesa').val()  ;
	var centroCusto = $('#select-centro-custo').val()  ;
	var justificativa = $('#justificativaDespesa').val();
	var reembolso = $('#select-tipo-lancamento').val();

	var regexNotEmpty = /\S/;

	if(!regexNotEmpty.test(dataDespesa)) {
		$('#dataDespesa').css('border-color', 'red');
		erro = true;
	} else {
		$('#dataDespesa').css('border-color', '');
	}

	var regexCurrency = /^\d+([.,]\d{1,2})?$/;

	if(!regexCurrency.test(valorDespesa)) {
		$('#valorDespesa').css('border-color', 'red');
		erro = true;
	} else {
		$('#valorDespesa').css('border-color', '');
	}
	if(!$('#div-qtd-km').hasClass('hide') && !regexCurrency.test(kmDespesa)) {
		$('#kmDespesa').css('border-color', 'red');
		erro = true;
	} else {
		$('#kmDespesa').css('border-color', '');
	}

	if(!regexNotEmpty.test(tipoDespesa)) {
		$('#tipoDespesaDiv').css({'border': '1px solid red', 'padding-left': '1px'});
		erro = true;
	} else {
		$('#tipoDespesaDiv').css({'border': '', 'padding-left': ''});
	}
	if(!regexNotEmpty.test(reembolso)) {
		$('#tipoLancamentoDiv').css({'border': '1px solid red', 'padding-left': '1px'});
		erro = true;
	} else {
		$('#tipoLancamentoDiv').css({'border': '', 'padding-left': ''});
	}

	if(!regexNotEmpty.test(centroCusto)) {
		$('#centroCustoDiv').css({'border': '1px solid red', 'padding-left': '1px'});
		erro = true;
	} else {
		$('#centroCustoDiv').css({'border': '', 'padding-left': ''});
	}

	if(!regexNotEmpty.test(justificativa)) {
		$('#justificativaDespesa').css('border-color', 'red');
		erro = true;
	} else {
		$('#justificativaDespesa').css('border-color', '');
	}

	if (!erro) {
		enviarFormDespesa();
	}
}

function enviarFormDespesa() {
	var oMyForm = new FormData();
	oMyForm.append("id", $('#id').val());
	oMyForm.append("comprovante", $('input[type=file]')[0].files[0]);
	oMyForm.append("dataOcorrencia", $('#dataDespesa').val());
	oMyForm.append("valor", $('#valorDespesa').val().replace(/,/g, '.'));
	oMyForm.append("idTipoDespesa", $('#select-tipo-despesa').val()  );
	oMyForm.append("idProjeto", $('#select-centro-custo').val()  );
	oMyForm.append("idFuncionario", $('#funcionario').val()         );
	oMyForm.append("justificativa", $('#justificativaDespesa').val());
	oMyForm.append("caminhoComprovante", $('#caminhoComprovante').val());
	oMyForm.append("reembolso",$('#select-tipo-lancamento').val());
	oMyForm.append("km", $('#kmDespesa').val().replace(/,/g, '.'));
	
	$.ajax({
		url : 'despesa',
		type: "POST",
		data : oMyForm,
		processData: false,
		contentType: false,
		enctype: 'multipart/form-data',
		success: function(data){
			var tipoLancamento = data.reembolso? 'Reembolso':'Justificativa';
			
			$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-success alert-dismissable");
			$("#span-msg").html('Despesa salva com sucesso!');
			setTimeout(function () { $('#div-alert').hide()}, 3000);
			$('#add-despesa-modal').modal("hide");
			if ($("#trDespesa" + data.id).length > 0) {
				$("#tdDataOcorrencia" + data.id).html(data.dataOcorrencia);
				$("#tdTipoLancamento"+data.id).html(tipoLancamento);
				$("#tdNomeTipoDespesa" + data.id).html(data.nomeTipoDespesa);
				$("#tdDescProjeto" + data.id).html(data.descricaoProjeto);
				$("#tdValor" + data.id).html('R$ ' + Number(data.valor).toFixed(2).replace(".",","));
				$("#tdNomeStatus" + data.id).html('<span class="label-status label-status-analise">'+ data.nomeStatus +'</span>');
			} else {
				$('#table-despesas tbody').prepend('<tr id="trDespesa'+ data.id +'">'
						+ '<td>' +  data.id+ '</td>'
						+ '<td id="tdDataOcorrencia'+ data.id +'">' +  data.dataOcorrencia+ '</td>'
						+ '<td id="tdTipoLancamento'+ data.id +'">' +  tipoLancamento+ '</td>'
						+ '<td id="tdNomeTipoDespesa'+ data.id +'">' + data.nomeTipoDespesa + '</td>'
						+ '<td id="tdDescProjeto'+ data.id +'">' + data.descricaoProjeto + '</td>'
						+ '<td id="tdValor'+ data.id +'">R$ ' + Number(data.valor).toFixed(2).replace(".",",") + '</td>'
						+ '<td id="tdNomeStatus'+ data.id +'"><span class="label-status label-status-analise">' + data.nomeStatus + '</span></td>'
						+ '<td>Aprovação do gestor</td>'
						+ '<td>' + '<a href="#" onclick="abrirModalEdicaoSolicitante(' + data.id + ', 1)">'
														+ '<i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>'
						+ '</tr>');
			}
				resetForm();
		},
		error: function(erro){
			if (erro.status === 403) {
				location.reload();
			} else {
				$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-danger alert-dismissable");
				$("#span-msg").html(erro.status + " - Despesa não pôde ser salva, tente novamente!");
				setTimeout(function () { $('#div-alert').hide()}, 3000);
				$('#add-despesa-modal').modal("hide");
			}
		}
	});
}

function resetForm() {
	$('#form-despesa')[0].reset();

	$("#select-tipo-despesa option").remove();
	$('#select-tipo-despesa').attr('disabled',true);
	$("#select-tipo-despesa").selectpicker("refresh");
	$("#select-centro-custo").val('default');
	$("#select-centro-custo").selectpicker("refresh");
	$("#select-tipo-lancamento").val('default');
	$("#select-tipo-lancamento").selectpicker("refresh");
	alternarCampoQtdKm('#select-tipo-despesa');
}

function enviarFormAnalise(idDespesa, fgFinanceiroGP, despesaAprovada, justificativa, reembolso) {
	reembolso = reembolso == undefined? false: reembolso;
	var oMyForm = new FormData();
	oMyForm.append("idDespesa", idDespesa);
	oMyForm.append("fgFinanceiroGP", fgFinanceiroGP);
	oMyForm.append("despesaAprovada", despesaAprovada);
	oMyForm.append("justificativa", justificativa);
	
	$.ajax({
		url : 'analisar',
		type: "POST",
		data : oMyForm,
		processData: false,
		contentType: false,
		success: function(data){
			$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-success alert-dismissable");
			$("#span-msg").html(data);
			setTimeout(function () { $('#div-alert').hide()}, 3000);
			if(!reembolso || fgFinanceiroGP == 'G'){
				$('#despesa' + idDespesa).remove();
			}
			if($("#table-despesas tr td").length <= 0) {
				 $('#table-despesas tbody').append('<tr><td colspan="6">Não há despesas para análise</td></tr>');
			}
			$('#detalhe-despesa-modal').modal("hide");
		},
		error: function(erro){
			if (erro.status === 403) {
				location.reload();
			} else {
				$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-danger alert-dismissable");
				$("#span-msg").html("Erro! " + erro.status);
				setTimeout(function () { $('#div-alert').hide()}, 3000);
				$('#detalhe-despesa-modal').modal("hide");
			}
		}
	});
}
function confirmarPagamento(idDespesa) {
	var oMyForm = new FormData();
	oMyForm.append("idDespesa", idDespesa);

	$.ajax({
		url : 'analisar',
		type: "PUT",
		data : oMyForm,
		processData: false,
		contentType: false,
		success: function(data){
			$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-success alert-dismissable");
			$("#span-msg").html(data);
			setTimeout(function () { $('#div-alert').hide()}, 3000);
			$('#despesa' + idDespesa).remove();
			if($("#table-despesas tr td").length <= 0) {
				 $('#table-despesas tbody').append('<tr><td colspan="6">Não há despesas para análise</td></tr>');
			}
			$('#detalhe-despesa-modal').modal("hide");
		},
		error: function(erro){
			if (erro.status === 403) {
				location.reload();
			} else {
				$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-danger alert-dismissable");
				$("#span-msg").html("Erro! " + erro.status);
				setTimeout(function () { $('#div-alert').hide()}, 3000);
				$('#detalhe-despesa-modal').modal("hide");
			}
		}
	});
}
function abrirModalVisualizacaoDespesa(idDespesa) {
	$.ajax({
		url: idDespesa,
		type: "get", 
		success: function(data) {
			$("#dataOcorrencia").val(data.dataOcorrencia);
			$("#solicitante").val(data.nomeFuncionario);
			$("#valorDespesa").val("R$ "+parseFloat(data.valor).toFixed(2));
			$("#tipoDespesa").val(data.nomeTipoDespesa);
			$("#projeto").val(data.descricaoProjeto);
			$("#tipoLancamento").val(data.reembolso? 'Reembolso':'Justificativa');
			$("#justificativaDespesa").val(data.justificativa);
			
			if(data.km && data.nomeTipoDespesa.toLowerCase() == "km"){
				$("#div-qtd-km").removeClass('hide');
				$("#kmDespesa").val(Number(data.km).toFixed(2).replace(".",","))
			}else{
				$("#div-qtd-km").addClass('hide');
			}
			if(!data.pago && data.idFinanceiroAprovador && data.idGpAprovador){
				$('#buttonSubmitFinanceiroDiv').addClass('hide');
				$('#buttonPagarFinanceiroDiv').removeClass('hide');
				$("#pagarDespesa").click(function(){
					confirmarPagamento(data.id);
				});
			}else{
				$('#buttonSubmitFinanceiroDiv').removeClass('hide');
				$('#buttonPagarFinanceiroDiv').addClass('hide');
			}
			if(data.reembolso){
				$("#btnDownloadRelComprovante").show();
				$("#btnDownloadRelComprovante").attr("href", "http://" + window.location.host + "/pause/despesa/relatorio-comprovante/" + data.id);
			}else{
				$("#btnDownloadRelComprovante").hide();
			}
			if (data.caminhoComprovante != null) {
				$("#btnDownloadArquivo").show();
				$("#btnDownloadArquivo").attr("href", "http://" + window.location.host + "/pause/despesa/arquivo/" + data.id);
			} else {
				$("#btnDownloadArquivo").hide();
				$("#btnDownloadArquivo").hide();
			}
			
			$("#aprovarDespesa").off("click");
			$("#aprovarDespesa").click(function(){
				if ($("#buttonSubmitGestorDiv").length) {
					enviarFormAnalise(data.id, 'G', true, "", data.reembolso);
				} else if ($("#buttonSubmitFinanceiroDiv").length) {
					enviarFormAnalise(data.id, 'F', true, "", data.reembolso);
				}
			});
			$("#aprovarDespesa").on("click");
			
			$("#rejeitarDespesa").off("click");
			$("#rejeitarDespesa").click(function(){
				
				$('#detalhe-despesa-modal').modal('hide');
				setTimeout(function() {
					$('#justificativa-modal').modal();
				}, 175);
				
				$("#enviarJustificativaReject").off("click");
				$("#enviarJustificativaReject").click(function(){
					var justReject = $('#justificativaReject').val();
					var regexNotEmpty = /\S/;
					
					if ($("#buttonJustRejectGestor").length) {
						
						if(regexNotEmpty.test(justReject)) {
							enviarFormAnalise(data.id, 'G', false, $("#justificativaReject").val());
							$('#justificativa-modal').modal('hide');
							resetFormJustificativa();
						} else {
							$('#justificativaReject').css('border-color', 'red');
							setTimeout(function() {
								$('#justificativaReject').css('border-color', '');
							}, 400);
						} 
						
					} else if ($("#buttonJustRejectFin").length) {
						
						if(regexNotEmpty.test(justReject)) {
							enviarFormAnalise(data.id, 'F', false, $("#justificativaReject").val());
							$('#justificativa-modal').modal('hide');
							resetFormJustificativa();
							
						} else {
							
							$('#justificativaReject').css('border-color', 'red');
							setTimeout(function() {
								$('#justificativaReject').css('border-color', '');
							}, 400);
						} 
					}
				});
			});
			$("#rejeitarDespesa").on("click");
			$('#detalhe-despesa-modal').modal();
		},
		error: function(erro) {
			if (erro.status === 403) {                        
				location.reload();                            
			} else {
				$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-danger alert-dismissable");
				$("#span-msg").html(erro.status + " - Despesa não pode ser visualizada, tente novamente!");
				setTimeout(function () { $('#div-alert').hide()}, 3000);
			}                                                 
		}
	});
}

function resetFormJustificativa() {
	$('#form-justificativa')[0].reset();
}

function abrirModalEdicaoSolicitante(idDespesa, abreDownload) {
	$.ajax({
		url: "despesa/"+idDespesa,
		type: "get", 
		success: function(data) {
			carregarTipoDespesa(data.idProjeto, function(){
				$("#id").val(data.id);
				$("#dataDespesa").val(data.dataOcorrencia.split("/").reverse().join("-"));
				$("#valorDespesa").val(Number(data.valor).toFixed(2).replace(".",","));
				$('#select-tipo-despesa').selectpicker('val', data.idTipoDespesa);
				$('#select-tipo-lancamento').selectpicker('val', data.reembolso+"");
				$('#select-centro-custo').selectpicker('val', data.idProjeto);
				$("#projeto").val(data.descricaoProjeto);
				$("#justificativaDespesa").val(data.justificativa);
				$("#caminhoComprovante").val(data.caminhoComprovante);
				$("#kmDespesa").val(Number(data.km).toFixed(2).replace(".",","));
				if (data.caminhoComprovante != null && abreDownload == 1) {
					$("#btnDownloadArquivo").show();
					$("#btnDownloadArquivo").attr("href", "http://" + window.location.host + "/pause/despesa/arquivo/" + data.id);
				} else {
					$("#btnDownloadArquivo").hide();
					$("#btnDownloadArquivo").hide();
				}
				if(data.reembolso){
					$("#btnDownloadRelComprovante").show();
					$("#btnDownloadRelComprovante").attr("href", "http://" + window.location.host + "/pause/despesa/relatorio-comprovante/" + data.id);
				}else{
					$("#btnDownloadRelComprovante").hide();
				}
				$("#modal-title").html('Editar Despesa');
				alternarCampoQtdKm('#select-tipo-despesa');
				
				$('#add-despesa-modal').modal();
			});
		},
		error: function(erro) {
			if (erro.status === 403) {                        
				location.reload();                            
			} else {
				$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-danger alert-dismissable");
				$("#span-msg").html(erro.status + " - Despesa não pode ser visualizada, tente novamente!");
				setTimeout(function () { $('#div-alert').hide()}, 3000);
			}                                                 
		}
	});
	
}

function abrirModalVisualizacaoSolicitante(idDespesa) {
	$.ajax({
		url: "despesa/"+idDespesa,
		type: "get", 
		success: function(data) {
			
			if(data.idStatus == 1) {
				$("#statusExib").attr("class","label-status label-status-analise");
			} else if (data.idStatus == 2) {
				$("#statusExib").attr("class","label-status label-status-aprovado");
			} else {
				$("#statusExib").attr("class","label-status label-status-reprovado");
			}
			
			$("#statusExib").html(data.nomeStatus);   
			$("#dataOcorrenciaExib").val(data.dataOcorrencia);
			$("#valorDespesaExib").val("R$ " + Number(data.valor).toFixed(2).replace(".",","));
			$("#TipoDespesaExib").val(data.nomeTipoDespesa);
			$("#TipoLancamentoExib").val(data.reembolso? 'Reembolso':'Justificativa');
			if(data.km && data.nomeTipoDespesa.toLowerCase() == "km"){
				$("#div-qtd-km-exib").removeClass('hide');
				$("#kmDespesaExib").val(Number(data.km).toFixed(2).replace(".",","))
			}else{
				$("#div-qtd-km-exib").addClass('hide');
			}
			$("#projetoExib").val(data.descricaoProjeto);
			$("#justificativaDespesaExib").val(data.justificativa);
			$("#justificativaRejecExib").val(data.justRejeicao);
			
			if(data.justRejeicao != null){
				$("#div-just-reject").attr("style", "display: block;");
			} else {
				$("#div-just-reject").attr("style", "display: none;");
			}
			
			if (data.caminhoComprovante != null) {
				$("#btnDownloadArquivoExib").show();
				$("#btnDownloadArquivoExib").attr("href", "http://" + window.location.host + "/pause/despesa/arquivo/" + data.id);
			} else {
				$("#btnDownloadArquivoExib").hide();
				$("#btnDownloadArquivoExib").hide();
			}

			$('#exibe-despesa-modal').modal();
		},
		error: function(erro) {
			if (erro.status === 403) {                        
				location.reload();                            
			} else {
				$("#div-alert").attr("style", "display: block;").attr("class", "alert alert-danger alert-dismissable");
				$("#span-msg").html(erro.status + " - Despesa não pode ser visualizada, tente novamente!");
				setTimeout(function () { $('#div-alert').hide()}, 3000);
			}                                                 
		}
	});
}

$("#valorDespesa").maskMoney();
$("#kmDespesa").maskMoney();

$('#add-despesa-modal').on('hidden.bs.modal',function(){
	resetForm();
});

function hideDiv(){
	$('#div-alert').hide();
}