package br.com.verity.pause.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DespesaEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private StatusEntity status;
	private Long tipoDespesa;
	private String justificativa;
	private Double valor;
	private Date dataSolicitacao;
	private Date dataOcorrencia;
	private Long idProjeto;
	private Long idSolicitante;
	private Date dataAcaoGp;
	private Date dataAcaoFinanceiro;
	private Long idGpAprovador;
	private Long idFinanceiroAprovador;
	private String caminhoComprovante;
	private String justRejeicao;
	private Boolean reembolso;
	private Double km;
	private Boolean pago;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getTipoDespesa() {
		return tipoDespesa;
	}
	
	public void setTipoDespesa(Long tipoDespesa) {
		this.tipoDespesa = tipoDespesa;
	}
	
	public String getJustificativa() {
		return justificativa;
	}
	
	public void setJustificativa(String justificativa) {
		this.justificativa = justificativa;
	}
	
	public Double getValor() {
		return valor;
	}
	
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}
	
	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}
	
	public Long getIdProjeto() {
		return idProjeto;
	}
	
	public void setIdProjeto(Long idProjeto) {
		this.idProjeto = idProjeto;
	}
	
	public Long getIdSolicitante() {
		return idSolicitante;
	}
	
	public void setIdSolicitante(Long idSolicitante) {
		this.idSolicitante = idSolicitante;
	}
	
	public Date getDataAcaoGp() {
		return dataAcaoGp;
	}
	
	public void setDataAcaoGp(Date dataAcaoGp) {
		this.dataAcaoGp = dataAcaoGp;
	}
	
	public Date getDataAcaoFinanceiro() {
		return dataAcaoFinanceiro;
	}
	
	public void setDataAcaoFinanceiro(Date dataAcaoFinanceiro) {
		this.dataAcaoFinanceiro = dataAcaoFinanceiro;
	}
	
	public Long getIdGpAprovador() {
		return idGpAprovador;
	}
	
	public void setIdGpAprovador(Long idGpAprovador) {
		this.idGpAprovador = idGpAprovador;
	}
	
	public Long getIdFinanceiroAprovador() {
		return idFinanceiroAprovador;
	}
	
	public void setIdFinanceiroAprovador(Long idFinanceiroAprovador) {
		this.idFinanceiroAprovador = idFinanceiroAprovador;
	}
	public String getCaminhoComprovante() {
		return caminhoComprovante;
	}
	public void setCaminhoComprovante(String caminhoComprovante) {
		this.caminhoComprovante = caminhoComprovante;
	}
	public StatusEntity getStatus() {
		return status;
	}
	public void setStatus(StatusEntity status) {
		this.status = status;
	}
	public Date getDataOcorrencia() {
		return dataOcorrencia;
	}
	public void setDataOcorrencia(Date dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}
	public String getJustRejeicao() {
		return justRejeicao;
	}
	public void setJustRejeicao(String justRejeicao) {
		this.justRejeicao = justRejeicao;
	}
	public Boolean getReembolso() {
		return reembolso;
	}
	public void setReembolso(Boolean reembolso) {
		this.reembolso = reembolso;
	}
	public Double getKm() {
		return km;
	}
	public void setKm(Double km) {
		this.km = km;
	}
	public Boolean getPago() {
		return pago;
	}
	public void setPago(Boolean pago) {
		this.pago = pago;
	}
}
